import SearchBar from "../components/SearchBar/SearchBar";

const apiKey = 'DzxAuvRDe8xPAHU0zu9XpJ01-s8x8YeaoYGTobN4yOhUyC4zsbLmmkda97OYhURPpR3mL6RJIaJHs4s-pXBzJc8Zady83n5fM0MZcR3Av_2fGalzmaUG-wmCCyrwXHYx';

export const Yelp = {

    search(term, location, sortBy) {
        return fetch(
            `https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`,
            {
                headers: {
                    Authorization: `Bearer ${apiKey}`
                }
            }
        )
        .then(response => {
            return response.json()
        })
        .then(jsonResponse => {
            if(jsonResponse.businesses) {
                return jsonResponse.businesses.map(business => {
                   return {
                    id: business.id,
                    imageSrc: business.image_url,
                    name: business.name,
                    address: business.address1,
                    city: business.location.address1,
                    state: business.location.state,
                    zipCode: business.location.zip_code,
                    category: business.categories.title,
                    rating: business.rating,
                    reviewCount: business.review_count
                   }
                })
            }
        })
    }
};